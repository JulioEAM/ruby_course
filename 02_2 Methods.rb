# Optional parameters

def say(*words)
  puts words
end
say("how","are","you")

# Returning values

def sum(a, b)
  res = a+b
end

x = sum(5, 23)
puts x

# Chaining methods
def square(x)
  x*x
end

square(2).times {puts "Hi"}

# Recursion
def fact(n)
  if n<= 1
    1
  else
    n * fact( n - 1 )
  end
end

puts fact(500)
# outputs 120

def fib(num)
  if num < 2
    num
  else
    fib(num-1) + fib(num-2)
  end
end
puts fib(4)