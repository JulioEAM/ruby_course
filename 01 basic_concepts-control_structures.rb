puts "Hello Ruby!"
print "Hello Ruby without new line after text"

puts "Hi"
print "there"
print "Ruby"

=begin
  este es un bloque de comentarios
  entonces todo lo que se encuentre
  enmedio será ignorado al momento de ejecutar el programa
=end

# Swapping variables

x = 10
y = 42
x, y = y, x

puts "\nx: #{x}\ny: #{y}\n"

# Getting input from user:
print "\nEscribe cualquier cosa y presiona ENTER:"
x = gets
puts x

# Getting input from user without adding a new line:
print "\nEscribe tu nombre completo y presiona ENTER: "
name = gets.chomp
puts "Welcome, #{name}\n"

puts 7.eql?(7.0)
puts 7 == 7.0

# Does OR operator make second comparison if the first one is true?

a = 5
b = 0

if a == 5 || b = 3
  puts "Fueron true"
end

puts "b: #{b}"

# ...The endind value of b is still 0 so if left part from an OR operator is true, there is no need
# to make the right side, so Ruby doesn't make it

