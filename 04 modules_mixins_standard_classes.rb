  class Bikes
    # This line will include a module named Comparable (It is already writen in Ruby and you can use it)
    include Comparable

    attr_accessor :make, :model, :color, :shifter, :wheel_size
    attr_reader :number_of_wheels, :kind
    attr_writer :serial_num
  
    def initialize(make,model,year,color, shifter, wheel_size)
      @number_of_wheels = 2
      @make = make
      @model = model
      @year = year
      @color = color
      @shifter = shifter
      @wheel_size = wheel_size
    end
    # This method will allow Bikes objs to compare it's wheel size and to know wich one is biger
    def <=>(other_obj)
      self.wheel_size <=> other_obj.wheel_size
    end
    
    def go_around
      puts "Going around is nicer in my bike !!!"
    end
  
    def to_s
      puts "You have a nice #{kind} bike made by #{@make} named #{@model} in #{@year}"
      puts "It has a beautiful #{@color} color and its wheels are #{@wheel_size}' size"
    end
  end
  
  class MTBBike < Bikes
    def initialize(make,model,year,color, shifter, wheel_size)
      super(make,model,year,color, shifter, wheel_size)
      @kind = "MTB"
    end
  
    def go_around
      puts "Going to a mountain is nicer on my bike !!!"
    end
  end
  
# Making Julio's bike object
  julio_s_bike = MTBBike.new("Specialized", "Hardrock", "2009", "gray and red", true, 26)
  p julio_s_bike.kind
  p julio_s_bike.go_around
  puts julio_s_bike

# Making Verónica's bike object
  vero_s_bike = MTBBike.new("Liv (Giant)", "Tempt", "2017", "green and bllue", true, 27.5)
  p vero_s_bike.kind
  p vero_s_bike.go_around
  puts vero_s_bike

# Using mixin Comparable 
  puts "Vero's bike has bigger wheels than Julio's bike" if vero_s_bike > julio_s_bike
  puts "Julio's bike has bigger wheels than Vero's bike" if julio_s_bike > vero_s_bike
  puts "Vero's bike and Julio's bike have the same size of wheels" if vero_s_bike == julio_s_bike

# Trying time
  # custom date
  t = Time.new(1988, 6, 10)
  # current time
  t = Time.now
  puts t
  # year, month, day
  puts t.year
  puts t.month
  puts t.day
  # day of week: 0 is Sunday
  puts t.wday
  # day of year
  puts t.yday

# Wraping block of code using Proc
  greet = Proc.new do |x|
    puts "Welcome #{x}"
  end

  greet.call "Julio"
  greet.call("Verónica")

  # A different and more complete example of Procs
  greet = Proc.new do |x|
    puts "Welcome #{x}"
  end
  
  goodbye = Proc.new do |x|
    puts "Goodbye #{x}"
  end
  
  def say(arr, proc)
    arr.each { |x| proc.call x}
  end

  people = ["David", "Amy", "John"]
  say(people, greet)
  say(people, goodbye)

# Taking time of a Proc execution
  def calc(proc)
    start = Time.now
    proc.call
    dur = Time.now - start
  end

  someProc = Proc.new do
    num = 0
    1000000.times do
      num = num + 1
    end
  end
  
  puts calc(someProc)