# Trying instance variables from outside the object

class Person
  def initialize(name)
    @name = name
  end
  def get_name
    @name
  end
end

p = Person.new("David")
puts p.get_name
# puts p.name             # This will fail because instance variables are not accessible from outside the object

# Setters in ruby_way
class Person2
  def initialize(name)
    @name = name
  end
  def get_name
    @name
  end

  def set_name=(name)
    @name = name
  end
end

p2 = Person2.new("Juan")
puts p2.get_name
p2.set_name = "Julio"
puts p2.get_name

# Setters named just like variable they are accessing, without words 'get' or 'set'

class Person3
  def initialize(name)
    @name = name
  end
  def name
    @name
  end
  def name=(name)
    @name = name
  end
end

p3 = Person3.new("Vero")
puts p3.name
p3.name = "Verónica"
puts p3.name

# Trying att_accessors

class Person4

  attr_accessor :name
  
  def initialize(name)
    @name = name
  end
end

p4 = Person4.new("David")
# Next 2 lines are possible because attr_accessor created methods def name and def name= as getter and setter respectively
p4.name = "Bob"
puts p4.name

#########

class RegularBike

  attr_accessor :kind, :color, :shifter, :wheel_size
  attr_reader :number_of_wheels
  attr_writer :serial_num

  def initialize
    @number_of_wheels = 2
  end
end

veronica_s_bike = RegularBike.new

p veronica_s_bike.number_of_wheels
p veronica_s_bike.kind
p veronica_s_bike.color
p veronica_s_bike.shifter
p veronica_s_bike.wheel_size
# This line will fail because serial_num has only it's setter but no getter (attr_writer)
# p veronica_s_bike.serial_num

veronica_s_bike.kind = 'MTB'
veronica_s_bike.color = 'Green/blue'
veronica_s_bike.shifter = true
veronica_s_bike.wheel_size = '27.5'
veronica_s_bike.serial_num = 1809347836

p veronica_s_bike.number_of_wheels
p veronica_s_bike.kind
p veronica_s_bike.color
p veronica_s_bike.shifter
p veronica_s_bike.wheel_size
# This line will fail because serial_num has only it's setter but no getter
# p veronica_s_bike.serial_num

# Class methods

class Bike
  def self.go_around
    puts "Going around is nicer in my bike !!!"
  end

  def not_a_class_method
    puts "This is not a class method so you need an instance of the Bike class to invoque it"
  end
end

Bike.go_around
# This line will fail because method is not a Class method
# Bike.not_a_class_method

# This line will fail
bike1 = Bike.new
#bike1.go_around
bike1.not_a_class_method

# Class variables (are accessible from every object of the class)
# Constants -> In the same example we will see how Constants work

class Bike2
  @@bike_count = 0
  WHEELS = true

  def initialize
    @@bike_count += 1
  end

  def self.get_count
    @@bike_count
  end
end

vero_s_bike = Bike2.new
julio_s_bike = Bike2.new
peluchosa_s_bike = Bike2.new

if Bike2::WHEELS
  p Bike2.get_count
else
  p "For some reason, bike does'nt has wheels"
end

# Inheritance
class Bikes
  attr_accessor :make, :model, :color, :shifter, :wheel_size
  attr_reader :number_of_wheels, :kind
  attr_writer :serial_num

  def initialize(make,model,year,color, shifter, wheel_size)
    @number_of_wheels = 2
    @make = make
    @model = model
    @year = year
    @color = color
    @shifter = shifter
    @wheel_size = wheel_size
  end
  
  def go_around
    puts "Going around is nicer in my bike !!!"
  end

  def to_s
    puts "You have a nice #{kind} bike made by #{@make} named #{@model} in #{@year}"
    puts "It has a beautiful #{@color} color and its wheels are #{@wheel_size}' size"
  end
end

class MTBBike < Bikes
  def initialize(make,model,year,color, shifter, wheel_size)
    super(make,model,year,color, shifter, wheel_size)
    @kind = "MTB"
  end

  def go_around
    puts "Going to a mountain is nicer in my bike !!!"
  end
end

julio_s_bike = MTBBike.new("Specialized", "Hardrock", "2009", "gray and red", true, "26")
p julio_s_bike.kind
p julio_s_bike.go_around
puts julio_s_bike


# Practicing Access Modifiers

class Player
  attr_accessor :name, :health, :power
  def initialize(n, h, pow)
    @name = n
    @health = h
    @power = pow
  end
  def isAlive
    @health > 0
  end
  def hit(opponent)
    opponent.health -= self.power
  end
  def to_s
    "#{name}: Health: #{health}, Power: #{power}"
  end
end

def smash(p1, p2)
  while p1.isAlive && p2.isAlive
    p1.hit(p2)
    p2.hit(p1)
    show_info(p1, p2)
    sleep(1)
  end
    
  if p1.isAlive 
    puts "#{p1.name} WON!" 
  elsif p2.isAlive
    puts "#{p2.name} WON!" 
  else
    puts "TIE!"
  end
end

def show_info(*p)
  p.each { |x| puts x}
end

def point_and_wait(c_return)
  3.times do |x|
    puts "." if c_return and x + 1 == 3
    print "." unless c_return and x + 1 == 3
    sleep(0.125)
  end
end

p1 = Player.new('Cloud', 101, 25)
p2 = Player.new('Luigi', 120, 20)
3.times do |x|
  point_and_wait(false)
  print x + 1
  point_and_wait(true)
end
puts "Smaaaash!!!"
sleep(0.750)
smash(p1, p2)