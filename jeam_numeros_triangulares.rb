def divisores(numero)
  cont = 0
  (1..Math.sqrt(numero).floor).each do |x|
    cont += 2 if (mod = numero % x) == 0 and (res_div = numero / x) != x
    cont += 1 if mod == 0 and res_div == x
  end
  cont
end

def triangulares(usr_divisores)
  ult_sum = 1
  triangular = 0
  num_divisores = 0
  until num_divisores > usr_divisores do
    triangular += ult_sum
    num_divisores = divisores(triangular)
    ult_sum += 1
  end
  triangular
end

print "De cuántos divisores quieres obtener el número triangular? : "
puts "Número triangular final: " + triangulares(usr_divisores = Integer(gets())).to_s