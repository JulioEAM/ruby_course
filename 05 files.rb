# Trying puts

log_file = File.new("logsesillo.txt","w+")
log_file.puts("Primer línea escrita en el archivo")
log_file.puts("Nueva escritura")
log_file.write(" ... en teoría, otra escritura pero sin salto de línea")
log_file.write(" ... Sí funciona!")
log_file.close

puts File.read("logsesillo.txt")

# With this block of code, Ruby ensure that file get closed at the end

# File.open("logsesillo.txt", "w+") {
#   |file| file.puts("Escritura después de cerrar el archivo...") 
# }

puts File.read("logsesillo.txt")


f = File.new("a.txt", "w+")
f.puts("1")
f.write("2")
f.puts("3")
f.close
puts File.read("a.txt")


File.readlines("logsesillo.txt").each {
  |line| puts " --- #{line}"
}

if File.file?("a.txt")  # It will validate if file exists
  File.delete("a.txt")
  puts "Se borró el archivo 'a.txt'"
else
  puts "No existía el archivo 'a.txt'"
end

# create a file
f = File.new("test.txt", "w+")
f.puts("some file content")

puts f.size # 19

f.close

puts File.zero?("test.txt") # false
puts File.readable?("test.txt") # true
puts File.writable?("test.txt")   # true
puts File.executable?("test.txt") # false