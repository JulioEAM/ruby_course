# Trying to insert new elements to an array

nombres = ['Julio', 'Verónica', 'Helena']
puts "\n #{nombres}"

nombres << 'Mary'
puts "\n #{nombres}"

nombres.push('Estefanía')
puts "\n #{nombres}"

nombres.insert(2,'Armando')
puts "\n #{nombres}"

# Trying to delete elements from an array

nombres.pop
puts "\n #{nombres}"

nombres.delete_at(2)
puts "\n #{nombres}"

# Some methods for hashes

bike_wish_list = { flat_pedals: "CrankBrothers", doubsusp_bike: "SantaCruz", full_helmet: "Leat", globes: "Leat"}
bike_wish_list.default = "new_wish?, add it to the list"

puts "\n #{bike_wish_list}"
puts "\n #{bike_wish_list.key("Leat")}" # Didn't return every keys matching value, only the first one
puts "\n #{bike_wish_list.invert}"      # Overwrite keys repeated; only left the last one
puts "\n #{bike_wish_list.keys}"
puts "\n #{bike_wish_list.values}"
puts "\n #{bike_wish_list.length}"
puts "\n Deleted value for key globes: #{bike_wish_list.delete(:globes)}"
puts "\n #{bike_wish_list}"

# Iterating arrays
arr = [2, 4, 6]
sum = 0
arr.each do |x|
  sum += x
end

puts sum # 12

# Testing default in an array of hashes
puts "\n #{bike_wish_list[:hola]}"

# Exercise with char counting
text = "Not resting until I get a SantaCruz bike"
text.downcase!
freqs = {}
freqs.default = 0

text.each_char { |char| freqs[char] += 1}

("a".."z").each {|x| puts "#{x} : #{freqs[x]}" }

# Printing what's really contained in freqs

freqs.each { |letter, freq| puts "Found letter #{letter} #{freq} times." }

